#include <stdio.h>
int sumbyvalue(int a, int b){
  a = 25;
  b = 25;
  return a+b;
}

int sumbyrefrence(int *a, int *b){
  *a = 25;
  *b = 25;
  return *a + *b;
}

int main(){
  int a = 3;
  int b = 5;
  int result;
  result = sumbyvalue(a, b);
  printf(" %d and %d By Value: %d\n", a, b, result );
  result = sumbyrefrence(&a, &b);
  printf(" %d and %d By Reference: %d\n", a, b, result );
  
  return 0;
}
