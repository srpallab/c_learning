#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 4

int position[N];
int p_choices[N][6];

int rand1(int);
void init_lucky_number(int);
void init(void);
int move(int);
void print(void);

int main(){
  int playerNo = 0;
  init();
  while (! move(playerNo)) {
    print();
    // printf("%d\n", playerNo);
    playerNo = (playerNo + 1) % N;
  }
  print();
  printf("Player NO %d is the winner.\n", playerNo);
  return 0;
}

int rand1(int n){
  return 1 + rand() / (((unsigned int)RAND_MAX + 1) / n );
}

void init_lucky_number(int playerNo){
  int i, j, k;
  const int multipliers[] = {1, 1, 1, 2, 2, 3};
  for (i = 0; i < 6; i++) {
    p_choices[playerNo][i] = 0;
  }
  for (j = 6; j > 0; j--) {
    //printf("RAND_MAX: %d rand(): %d\n", (unsigned int)RAND_MAX, rand());
    k = rand1(j);
    printf("randl(%d): %d\n", j, k);
    for (i = 0; i < 6; i++) {
      if (p_choices[playerNo][i] == 0) {
	k--;
	if (k == 0) {
	  p_choices[playerNo][i] = multipliers[j-1];
	  printf("p_cohices[%d][%d] = multipliers[%d]\n",
		 playerNo, i, j-1);
	  break;
	}
      }
    }
  }
}

void init(void){
  int i;
  /* seed the rand function with srand */
  srand(time(NULL));
  for (i = 0; i < N; i++) {
    position[i] = 1;
    printf("Player: %d\n", i);
    init_lucky_number(i);
  }

}

int move(int playerNo){
  int base_steps = rand1(6);
  int multiplier = p_choices[playerNo][rand1(6) - 1];
  int steps = base_steps * multiplier;
  // printf("%d\n", steps);
  int i;
  position[playerNo] += steps;
  if (position[playerNo] >= 50) {
    position[playerNo] = 50;
    return 1;
  }
  for (i = 0; i < N; ++i) {
    if (i != playerNo && position[i] == position[playerNo]) {
      position[i] = 1;
      break;
    }
  }
  return 0;
}


void print(void){
  char status[] = "S--------|---------|---------|---------|--------G";
  int playerNo;
  int sqareNo;
  //printf("%c\n", status[10]);
  for (playerNo = N - 1; playerNo >= 0; playerNo--) {
    sqareNo = position[playerNo] - 1;
    if (status[sqareNo] == '-' || status[sqareNo] == '|') {
      // printf("%d\n", sqareNo);
      status[sqareNo] = playerNo + '0';
    }
  }
  printf("%s\n", status);
}
